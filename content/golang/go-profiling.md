---
title: "Go Profiling"
date: 2019-01-18T14:05:11+11:00
draft: false
---


# Table of Contents

1.  [Summary](#orgcdad964)
2.  [Initiating pprof endpoints in application](#orgc7ce9c3)
3.  [Profiling in a fancy way (using go-torch)](#org9f1e87e)
    1.  [Non Go 1.11](#org5180766)
    2.  [Go 1.11](#org520e725)
4.  [Profiling the hard way](#org11d3a9a)
5.  [Reference](#orgbc285d3)



<a id="orgcdad964"></a>

# Summary

Profiling application is important as it could help to visualize application memory usage and identify bottlenecks.
Go tool pprof is an integrated tool in golang and it could run directly in production environment. 


<a id="orgc7ce9c3"></a>

# Initiating pprof endpoints in application
Firstly, I could add a *debug* routing in *letterbox.com*  Golang application. To restrict access, I could add some middlewares checking to whitelist certain IPs.
```
    func addPprof(e *echo.Echo) {
    	dbg := e.Group("/debug")
    	dbg.Use(middlewares.RestrictIPAddress("10.0.1.1")) // only restricted to specific ip
    	dbg.GET("/vars", echo.WrapHandler(http.DefaultServeMux))
    	dbg.GET("/pprof/*", echo.WrapHandler(http.DefaultServeMux))
    }
```

<a id="org9f1e87e"></a>

# Profiling in a fancy way (using go-torch)


<a id="org5180766"></a>

## Non Go 1.11
```
    go get -u github.com/google/pprof
    pprof -http=":8081"  http://letterbox.com/debug/pprof/heap
```

<a id="org520e725"></a>

## Go 1.11
```
    go tool pprof -http=":8081"  http://letterbox.com/debug/pprof/heap
```
You will get a fancy graph at:
<http://localhost:8081/ui/flamegraph>
Remember this is running on your computer/dedicated server where you can expose port 8081(or any port number). 


<a id="org11d3a9a"></a>

# Profiling the hard way

There are 3 endpoints to expose profiling:

-   Heap profile: <http://letterbox.com/debug/pprof/heap>
    -   inuse<sub>space</sub>(default)
    -   inuse<sub>objects</sub>
    -   alloc<sub>space</sub>
    -   alloc<sub>objects</sub>
-   Goroutine blocking profile: <http://letterbox.com/debug/pprof/block>
-   CPU Profile(running in 30s): <http://letterbox.com/debug/pprof/profile>

Run these commands in terminal to access 
```
    go tool pprof --alloc_space http://letterbox.com/debug/pprof/heap      # heap profile
    go tool pprof http://letterbox.com/debug/pprof/block                   # goroutine blocking profile
    go tool pprof http://letterbox.com/debug/pprof/profile                 # 30-second CPU profile
    go tool pprof http://letterbox.com/debug/vars                          # see free variables
```
Once you are in `pprof` prompt, there are few commands to use:

-   top5: list of top 5 processes
-   top10: list of top 10 processes
-   list [func name]: debug the allocated memory of name of func/process in the above 2 commands.
-   web: see the whole graph in svg format
-   png > graph.png: export to png file
-   svg > graph.svg: export to svg file

<a id="orgbc285d3"></a>

# Reference

-   <https://github.com/uber/go-torch>
-   <https://blog.golang.org/profiling-go-programs>

