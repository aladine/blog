---
title: "Goland Tips and Tricks"
date: 2018-12-20T11:55:18+11:00
draft: false
---
# Popular keyboard shortcuts

| Action        | Description           |
| ------------- |-------------|
| Move the current line of code | ⇧⌘↑ ⇧⌘↓|
| Duplicate a line of code| ⌘D|
| Remove a line of code | ⌘⌫|
| Comment or uncomment a line of code | ⌘/|

# Use Live Templates

When you want to add snippet to golang collection, select the code block.
Then press ⌘ + Shift + A, search for Save as Live Template. 
It will promt a dialog for you to edit the snippet.

# Use gofmt and goimports after saving files
After saving *.go* files, perhaps you want to run ~gofmt~ and ~goimports~ automatically. File Watchers option will help to achieve this:

- Go to Tools > File Watchers 
- Click on + icon to add a new template:
  + go fmt
  + goimports
  + gomenalinter
- Save and now you are done

![Goland Screenshot](/img/goland1.png)
