---
title: "Struct Size"
date: 2019-01-13T18:52:26+11:00
draft: true
---

# Run aligncheck

Struct can be optimize better
```
go get gopkg.in/alecthomas/gometalinter.v1
gometalinter.v1 --install
gometalinter.v1 --disable=gocyclo --disable=vetshadow --disable=goconst --disable=gotype --disable=errcheck --json ./...
```

# Empty struct
The empty struct is a struct type that has no fields.

Empty struct is powerful. It require 0 bytes to allocates in memory. Hence, it is widely used in many packages to define a base type of struct. 

Interestingly, two empty structs have the same addresses
```
a := struct{}{} // not the zero value, a real new struct{} instance
b := struct{}{}
fmt.Println(a == b) // true
```




# Reference:
- http://golang-sizeof.tips/
- https://dave.cheney.net/2014/03/25/the-empty-struct#comment-2815
