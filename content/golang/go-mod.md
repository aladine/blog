---
title: "Moving from go dep vendor to go module"
date: 2018-12-23T00:27:42+11:00
draft: false
---

This post show how easy it is to move from Go Dep to Go Module.

Minimum version of golang for Go Mod: Go `1.11`

# What is the main difference

Go Dep: 

- GoPkg.toml: Go packagages will be defined
- GoPkg.lock: Go packages version will be lock in this file
- vendors/: directory contains all the dependencies libraries.

Go Module: 

- Creating projects outside $GOPATH. It could be existed together with Go Dep
- go.mod: similar to GoPkg.toml
- go.sum: similar to GoPkg.lock

Go Module has 2 other advantages apart from built in feature of Go 1.11:

- It doesn't auto upgrade version for packages. 
- It allow multiple versions of the same package in the same app.

# Usage

Assume that i have *xerogolang* repository. To use go mod, simply run this command:
{{< highlight html >}}
go mod init https://github.com/XeroAPI/xerogolang
{{< /highlight >}}

You may encounter some errors if you run inside $GOPATH, you can export these 2 enviromental variables *GO111MODULE=on* and *GOCACHE=on* before running the command.


```
go build
go mod tidy
```

{{< highlight html >}}
> cat go.mod 
module github.com/XeroAPI/xerogolang

require (
	github.com/XeroAPI/xerogolang v0.0.0-20181009065450-346527cfbbda
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/pat v0.0.0-20180118222023-199c85a7f6d1
	github.com/gorilla/sessions v1.1.1
	github.com/markbates/goth v1.47.2
	github.com/mrjones/oauth v0.0.0-20180629183705-f4e24b6d100c
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	golang.org/x/oauth2 v0.0.0-20181017192945-9dcd33a902f4
)
{{< /highlight >}}


To add dependency for a single package, just run
```
 go get github.com/labstack/echo@'<v3.3.5'
 go get github.com/labstack/echo@v3.3.5
 go get github.com/labstack/echo
```

{{< highlight html >}}
> cat go.sum
cloud.google.com/go v0.30.0/go.mod h1:aQUYkXzVsufM+DwF1aE+0xfcU+56JwCaLick0ClmMTw=
github.com/XeroAPI/xerogolang v0.0.0-20181009065450-346527cfbbda h1:1539tHZ7fRKBUQ0wvQZ7VCf8xA4/h6T/L2mRot9wHJE=
github.com/XeroAPI/xerogolang v0.0.0-20181009065450-346527cfbbda/go.mod h1:U4pkrQIaEmL9/7LUW/bTSIj18Y9YCcO/xT3GGxcJ+XM=
github.com/davecgh/go-spew v1.1.1 h1:vj9j/u1bqnvCEfJOwUhtlOARqs3+rkHYY13jYWTU97c=
github.com/davecgh/go-spew v1.1.1/go.mod h1:J7Y8YcW2NihsgmVo/mv3lAwl/skON4iLHjSsI+c5H38=
...
{{< /highlight >}}

# Reference
Please check out [Golang Wiki](https://github.com/golang/go/wiki/Modules) for more info.
