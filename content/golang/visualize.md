---
title: "Visualize using dot"
date: 2018-12-26T22:54:57+11:00
draft: false
---

Dot is a plain text description language. It could draw diagram or graph from command line. 
There are some use cases to use `dot` to visualize graph or dependencies. 
Dot is installed in MacOS with graphiz.
```
brew install graphiz
```

# Golang package dependencies
If you are using godep in your package, you can export the dependencies map to either svg or png format:
```
  dep status -dot | dot -T svg > ~/Desktop/graph.svg | open -f -a /Applications/Firefox.app
  dep status -dot | dot -T png | open -f -a /Applications/Preview.app 
```

# Memory profile of golang application (pprof)
```
go tool pprof --inuse_space http://10.10.0.1:8098/debug/pprof/heap | dot -T png
go tool pprof --alloc_space http://10.10.0.1:8098/debug/pprof/heap | dot -T png
// this can be also achieved by pprof > web
```


# Finite state machine
```
digraph finite_state_machine {
	rankdir=LR;
	size="8,5"
	node [shape = circle];
	S0 -> S1 [ label = "Lift Nozzle" ]
	S1 -> S0 [ label = "Replace Nozzle" ]
	S1 -> S2 [ label = "Authorize Pump" ]
	S2 -> S0 [ label = "Replace Nozzle" ]
	S2 -> S3 [ label = "Pull Trigger" ]
	S3 -> S2 [ label = "Release Trigger" ]
}
```
![Output](http://www.tonyballantyne.com/img/petrol.png)

# Reference
- http://www.tonyballantyne.com/graphs.html
- https://blog.golang.org/profiling-go-programs
