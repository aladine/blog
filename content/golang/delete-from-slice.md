---
title: "Delete from Slice"
date: 2019-01-18T14:05:11+11:00
draft: true
---

Recently I found a code which was written by my colleague to delete an element from array
```go
	timecards = append(timecards[:i], timecards[i+1:]...)
```

It looks quite neat, but I found if we delete it many times, the array is moving a lot of times. There is a better way to handle it:
```go
		if i < len(timecards)-1 {
			copy(timecards[i:], timecards[i+1:])
		}
		timecards[len(timecards)-1] = models.Timecard{}
		timecards = timecards[:len(timecards)-1]
```
