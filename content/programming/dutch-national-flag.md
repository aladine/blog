---
title: "Dutch National Flag"
date: 2020-06-15T14:05:11+11:00
draft: false
---
The problem statement is similar to Dutch National Flag problem with 3 colors red, blue and white.

>Given an array with n objects colored red, white or blue, sort them in-place so that objects of the same color are adjacent, with the colors in the order red, white and blue.

>Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.

The idea to tackle this problem is to partition the array to different parts based on the value:
- a[1..l-1] zeroes
- a[l..m-1] ones
- a[m..h] unknown
- a[h+1..N] twos

Here is my solution. This is the first time I code in Kotlin with a functional style.
{{< gist aladine a96e9ac3c4f06f99da140aad954166e6 >}}
