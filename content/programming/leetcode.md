---
title: "5 Leetcode features you might not know about"
date: 2020-05-22T14:05:11+11:00
draft: false
---
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Mock test](#mock-test)
- [Topics](#topics)
- [Session](#session)
- [Forum/Discuss](#forumdiscuss)
- [Notes(notebook)](#notesnotebook)
- [What is not the right way to grind LC](#what-is-not-the-right-way-to-grind-lc)
    - [Spend too much time for hard problems](#spend-too-much-time-for-hard-problems)
    - [Spend too much time for easy/stubborn problems](#spend-too-much-time-for-easystubborn-problems)
    - [Not test in your local machine](#not-test-in-your-local-machine)
- [Conclusion](#conclusion)

<!-- markdown-toc end -->

# Mock test
Mock test simulates a real interview session with 2-5 problems set. It is useful if you have an upcoming interview for a specific company.

![](/img/leetcode/mock.png)

A real mock interview session consists of 3 types: 
- Online Assessment(short)
- Phone interview(medium)
- On-site (long)

Free users can pick a random mock test. Premium users will be able to choose the desired company with more closed to their real interview question.

After taking mock test, you will get a report of performance based on 2 metrics:

- How much you score compare to others
- Where are you in the leaderboards of users who finish the same test.
If you need to stop the test for some reasons, you can abandon the test and its final score won’t calculate to overall stats.

# Topics
A group of interview questions. It could be based on types of problems or top easy/medium/hard questions:
- Top 100 interview questions
- Top Easy interview problems
- Top Medium interview problems
- Top Hard interview problems
- Problem sets by company(premium)

Recently, Leetcode added monthly challenges which I found it quite cool. It is addictive to encourage you visiting LC every day. The current monthly challenge is [May Challenge](https://leetcode.com/explore/featured/card/may-leetcoding-challenge/)

# Session
Session is a period to measure Attempt/Completion statistics counter. Usually you should create a new session after a long time not accessing LC. The main purpose is to measure your current process. Some examples of session use case are:
- By languages (CPP, Java, Python...)
- By year/month
- By company

Problems could be practiced and tracked in progress separately in each created session. If you didn't practice LC for a while, it is better to start a new session to track the accepted/submitted questions/submissions.

# Forum/Discuss
Many problems have official solutions, but I feel the forum is more resourceful. Just browse and sort by votes. The best solutions are always on the top. You can join the discussion by adding a post. 

From my observation, you should add a new post when:
- You have an interesting avatar.
- Your solution is 100% memory efficient than others.
- The problem was created recently. If the problem is old, probably nobody will read your post. Instead, you could post your solution to one of the best upvoted existing threads. That will increase the chance others see and upvote your solution :)

# Notes(notebook)

This is an underrated feature of LC but I find it particularly useful. If you notice a little editor icon on the right of the page, that is where you could add a small note for each problem. I found that the notes are stored throughout all the sessions. So it could be very easy to print all these notes to pdf format and revise all at once.

![](/img/leetcode/discuss.png)

# What is not the right way to grind LC

## Spend too much time for hard problems

In my opinion, hard problems in LC are really hard. The high chances are you seldom encounter them in interview. If it does, maybe during onsite interview. Again, the expectation of a whiteboard interview and a compile-pass-test interview are different. So if you feel confident enough to solve hard problems, then you can start try it. Generally, you can try to solve hard problems after attempting trying all the easy and medium problems in LC. Another tip is you should sort the hard questions by frequency and pick the highest one.

## Spend too much time for easy/stubborn problems

Easy questions should be practice once a while. Your focus should be medium level questions.
Again, sometimes you may encounter a stubborn problem, which look easy but has too many edge cases. Just take a quick glance on the number of upvote/downvote. If there are too many down votes, it is highly likely it is a boring problem.

## Not test in your local machine
Leetcode UI will be good enough for simple problems, but for complicated ones, it is probably better to run more test cases in your machine first. 

My daily steps are:

- Copy code from leetcode code section and paste to my workspace.
- Add a main function with some boilterplate comments. Usually, I add this main func to run it
- Run make to make sure test passed or at least my code is compile. [Makefile](https://gist.github.com/aladine/3af34c36bb552b9abdc7eff87d273a4e)
- Copy code back to Leetcode and run test. This is to make sure no typo or error.
- Debug(premium only) and submit

# Conclusion
You may ask whether you should buy Leetcode(LC) premium. I even see posts on Reddit to share LC account. Will it worth?

If you dream about getting a job in FAANG and then you should think LC Premium is the smallest investment you can get the most return from it. Others are time and effort. So if you are still hesitant, just subscribe for 1 month and see how it go.
